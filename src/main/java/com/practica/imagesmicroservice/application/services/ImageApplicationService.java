package com.practica.imagesmicroservice.application.services;

import com.practica.imagesmicroservice.application.dtos.image.ImageCreationDTO;
import com.practica.imagesmicroservice.application.dtos.image.ImageResponseDTO;
import com.practica.imagesmicroservice.application.dtos.image.ImageUpdateDTO;

public interface ImageApplicationService {
    public ImageResponseDTO addImage(String personId, ImageCreationDTO imageCreationDTO);

    public ImageResponseDTO getImageByPersonId(String personId);

    public void updateImage(String personId, ImageUpdateDTO image);

    public void deleteImageByPersonId(String personId);
}
