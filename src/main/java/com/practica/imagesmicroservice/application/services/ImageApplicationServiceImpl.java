package com.practica.imagesmicroservice.application.services;

import com.practica.imagesmicroservice.application.dtos.image.ImageCreationDTO;
import com.practica.imagesmicroservice.application.dtos.image.ImageResponseDTO;
import com.practica.imagesmicroservice.application.dtos.image.ImageUpdateDTO;
import com.practica.imagesmicroservice.application.ports.ImageMapperService;
import com.practica.imagesmicroservice.domain.entity.image.Image;
import com.practica.imagesmicroservice.domain.usecases.AddImageUseCase;
import com.practica.imagesmicroservice.domain.usecases.DeleteImageUseCase;
import com.practica.imagesmicroservice.domain.usecases.GetImageUseCase;
import com.practica.imagesmicroservice.domain.usecases.UpdateImageUseCase;

public class ImageApplicationServiceImpl implements ImageApplicationService{

    private final ImageMapperService imageMapperService;

    private final AddImageUseCase addImageUseCase;

    private final GetImageUseCase getImageUseCase;

    private final UpdateImageUseCase updateImageUseCase;

    private final DeleteImageUseCase deleteImageUseCase;

    public ImageApplicationServiceImpl(ImageMapperService imageMapperService, AddImageUseCase addImageUseCase, GetImageUseCase getImageUseCase, UpdateImageUseCase updateImageUseCase, DeleteImageUseCase deleteImageUseCase) {
        this.imageMapperService = imageMapperService;
        this.addImageUseCase = addImageUseCase;
        this.getImageUseCase = getImageUseCase;
        this.updateImageUseCase = updateImageUseCase;
        this.deleteImageUseCase = deleteImageUseCase;
    }

    @Override
    public ImageResponseDTO addImage(String personId, ImageCreationDTO imageCreationDTO) {
        Image image = imageMapperService.imageCreationDtoToImage(imageCreationDTO);
        image.setPersonId(personId);
        Image savedImage = addImageUseCase.execute(image);
        return imageMapperService.imageToImageResponseDto(savedImage);
    }

    @Override
    public ImageResponseDTO getImageByPersonId(String personId) {
        Image image = new Image();
        image.setPersonId(personId);
        Image imageResult = getImageUseCase.execute(image);
        return imageMapperService.imageToImageResponseDto(imageResult);
    }

    @Override
    public void updateImage(String personId, ImageUpdateDTO imageUpdateDTO) {
        Image tempImage = new Image();
        tempImage.setPersonId(personId);
        Image oldImage = getImageUseCase.execute(tempImage);
        Image newImage = imageMapperService.ImageUpdateDtoToImage(imageUpdateDTO);
        Image imageUpdatedFields = imageMapperService.imageToImage(newImage, oldImage);
        updateImageUseCase.execute(imageUpdatedFields);
    }

    @Override
    public void deleteImageByPersonId(String personId) {
        Image tempImage = new Image();
        tempImage.setPersonId(personId);
        deleteImageUseCase.execute(tempImage);
    }
}
