package com.practica.imagesmicroservice.application.dtos.image;

import lombok.*;
import org.springframework.web.multipart.MultipartFile;

@Getter
@Setter
@ToString(includeFieldNames=true)
@AllArgsConstructor
public class ImageCreationDTO {

    @NonNull
    private MultipartFile file;
}
