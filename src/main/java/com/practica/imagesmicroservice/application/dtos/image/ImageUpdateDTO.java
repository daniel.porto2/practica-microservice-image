package com.practica.imagesmicroservice.application.dtos.image;

import lombok.*;
import org.springframework.web.multipart.MultipartFile;

@Getter
@Setter
@ToString(includeFieldNames=true)
@AllArgsConstructor
public class ImageUpdateDTO {

    @NonNull
    private MultipartFile file;
}
