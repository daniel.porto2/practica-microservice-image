package com.practica.imagesmicroservice.application.dtos.image;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ImageResponseDTO {
    private String id;

    private byte[] file;
}
