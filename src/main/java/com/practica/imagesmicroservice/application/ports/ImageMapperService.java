package com.practica.imagesmicroservice.application.ports;

import com.practica.imagesmicroservice.application.dtos.image.ImageCreationDTO;
import com.practica.imagesmicroservice.application.dtos.image.ImageResponseDTO;
import com.practica.imagesmicroservice.application.dtos.image.ImageUpdateDTO;
import com.practica.imagesmicroservice.domain.entity.image.Image;

public interface ImageMapperService {
    public Image imageCreationDtoToImage(ImageCreationDTO imageCreationDTO);

    public ImageResponseDTO imageToImageResponseDto(Image image);

    public Image ImageUpdateDtoToImage(ImageUpdateDTO imageUpdateDTO);

    public Image imageToImage(Image image1, Image image2);
}
