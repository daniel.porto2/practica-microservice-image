package com.practica.imagesmicroservice.domain.shared.mainexceptions;

public class DuplicatedFieldException extends RuntimeException{
    public DuplicatedFieldException(String message) {
        super(message);
    }
}
