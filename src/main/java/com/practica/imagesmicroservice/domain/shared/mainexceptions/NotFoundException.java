package com.practica.imagesmicroservice.domain.shared.mainexceptions;

public class NotFoundException extends RuntimeException{
    public NotFoundException(String message) {
        super(message);
    }
}
