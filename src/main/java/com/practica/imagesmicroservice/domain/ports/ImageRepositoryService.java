package com.practica.imagesmicroservice.domain.ports;

import com.practica.imagesmicroservice.domain.entity.image.Image;

public interface ImageRepositoryService {
    public Image saveImage(Image image);

    public Image getImage(Image image);

    public void updateImage(Image image);

    public void deleteImage(Image image);

    public boolean doesPersonIdHaveImage(String personId);
}
