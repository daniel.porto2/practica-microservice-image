package com.practica.imagesmicroservice.domain.ports;

public interface PeopleGatewayService {
    public Boolean doesPersonIdExists(String personId);
}
