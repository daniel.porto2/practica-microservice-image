package com.practica.imagesmicroservice.domain.entity.image;

public class Image {
    private String id;

    private byte[] file;

    private String personId;

    public Image(String id){
        this.id = id;
    }

    public Image() {

    }

    public Image(String id, byte[] file, String personId) {
        this.id = id;
        this.file = file;
        this.personId = personId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public byte[] getFile() {
        return file;
    }

    public void setFile(byte[] file) {
        this.file = file;
    }

    public String getPersonId() {
        return personId;
    }

    public void setPersonId(String personId) {
        this.personId = personId;
    }
}
