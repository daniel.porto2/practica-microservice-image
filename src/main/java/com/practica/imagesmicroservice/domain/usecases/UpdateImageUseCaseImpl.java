package com.practica.imagesmicroservice.domain.usecases;

import com.practica.imagesmicroservice.domain.entity.image.Image;
import com.practica.imagesmicroservice.domain.exceptions.ImageNotFoundException;
import com.practica.imagesmicroservice.domain.ports.ImageRepositoryService;

public class UpdateImageUseCaseImpl implements UpdateImageUseCase{

    private final ImageRepositoryService imageRepositoryService;

    public UpdateImageUseCaseImpl(ImageRepositoryService imageRepositoryService) {
        this.imageRepositoryService = imageRepositoryService;
    }

    @Override
    public void execute(Image image) {
        if(!imageRepositoryService.doesPersonIdHaveImage(image.getPersonId())){
            throw new ImageNotFoundException();
        }
        imageRepositoryService.updateImage(image);
    }
}
