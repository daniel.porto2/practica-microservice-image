package com.practica.imagesmicroservice.domain.usecases;

import com.practica.imagesmicroservice.domain.entity.image.Image;

public interface UpdateImageUseCase {
    public void execute(Image image);
}
