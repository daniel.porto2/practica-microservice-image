package com.practica.imagesmicroservice.domain.usecases;

import com.practica.imagesmicroservice.domain.entity.image.Image;

public interface DeleteImageUseCase {
    public void execute(Image image);
}
