package com.practica.imagesmicroservice.domain.usecases;

import com.practica.imagesmicroservice.domain.entity.image.Image;
import com.practica.imagesmicroservice.domain.exceptions.ImageNotFoundException;
import com.practica.imagesmicroservice.domain.ports.ImageRepositoryService;

public class DeleteImageUseCaseImpl implements DeleteImageUseCase {

    private final ImageRepositoryService imageRepositoryService;

    public DeleteImageUseCaseImpl(ImageRepositoryService imageRepositoryService) {
        this.imageRepositoryService = imageRepositoryService;
    }

    @Override
    public void execute(Image image) {
        if(!imageRepositoryService.doesPersonIdHaveImage(image.getPersonId())){
            throw new ImageNotFoundException("Image of person with id " + image.getPersonId() + " not found");
        }
        imageRepositoryService.deleteImage(image);
    }
}
