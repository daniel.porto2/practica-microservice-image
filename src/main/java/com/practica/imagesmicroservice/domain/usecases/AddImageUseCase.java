package com.practica.imagesmicroservice.domain.usecases;

import com.practica.imagesmicroservice.domain.entity.image.Image;

public interface AddImageUseCase {
    public Image execute(Image image);
}
