package com.practica.imagesmicroservice.domain.usecases;

import com.practica.imagesmicroservice.domain.entity.image.Image;
import com.practica.imagesmicroservice.domain.exceptions.ImageAlreadyExistsForPersonId;
import com.practica.imagesmicroservice.domain.exceptions.PersonIdNotFoundException;
import com.practica.imagesmicroservice.domain.ports.ImageRepositoryService;
import com.practica.imagesmicroservice.domain.ports.PeopleGatewayService;

public class AddImageUseCaseImpl implements AddImageUseCase{

    private final ImageRepositoryService imageRepositoryService;

    private final PeopleGatewayService PeopleGatewayService;

    public AddImageUseCaseImpl(ImageRepositoryService imageRepositoryService, PeopleGatewayService PeopleGatewayService) {
        this.imageRepositoryService = imageRepositoryService;
        this.PeopleGatewayService = PeopleGatewayService;
    }

    public Image execute(Image image) {
        if(!PeopleGatewayService.doesPersonIdExists(image.getPersonId())){
            throw new PersonIdNotFoundException();
        }
        if(imageRepositoryService.doesPersonIdHaveImage(image.getPersonId())){
            throw new ImageAlreadyExistsForPersonId();
        }
        return imageRepositoryService.saveImage(image);
    }
}
