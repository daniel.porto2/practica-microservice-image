package com.practica.imagesmicroservice.domain.exceptions;

import com.practica.imagesmicroservice.domain.shared.mainexceptions.DuplicatedFieldException;

public class ImageAlreadyExistsForPersonId extends DuplicatedFieldException {
    public static final String DESCRIPTION = "An image already exists for this person";

    public ImageAlreadyExistsForPersonId() {
        super(DESCRIPTION);
    }

    public ImageAlreadyExistsForPersonId(String detail) {
        super(DESCRIPTION + ". " + detail);
    }
}
