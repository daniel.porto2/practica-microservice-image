package com.practica.imagesmicroservice.domain.exceptions;

import com.practica.imagesmicroservice.domain.shared.mainexceptions.NotFoundException;

public class ImageNotFoundException extends NotFoundException {
    public static final String DESCRIPTION = "Image not found";

    public ImageNotFoundException() {
        super(DESCRIPTION);
    }

    public ImageNotFoundException(String detail) {
        super(DESCRIPTION + ". " + detail);
    }
}
