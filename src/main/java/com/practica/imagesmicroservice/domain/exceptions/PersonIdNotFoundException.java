package com.practica.imagesmicroservice.domain.exceptions;

import com.practica.imagesmicroservice.domain.shared.mainexceptions.NotFoundException;

public class PersonIdNotFoundException extends NotFoundException {
    public static final String DESCRIPTION = "Person Id not found";

    public PersonIdNotFoundException() {
        super(DESCRIPTION);
    }

    public PersonIdNotFoundException(String detail) {
        super(DESCRIPTION + ". " + detail);
    }
}
