package com.practica.imagesmicroservice.infraestructure.adapters.mappers;

import com.practica.imagesmicroservice.application.dtos.image.ImageCreationDTO;
import com.practica.imagesmicroservice.application.dtos.image.ImageResponseDTO;
import com.practica.imagesmicroservice.application.dtos.image.ImageUpdateDTO;
import com.practica.imagesmicroservice.application.ports.ImageMapperService;
import com.practica.imagesmicroservice.domain.entity.image.Image;
import org.modelmapper.ModelMapper;

import java.io.IOException;

public class ImageMapperServiceImpl implements ImageMapperService {

    private final ModelMapper modelMapper;

    public ImageMapperServiceImpl(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    @Override
    public Image imageCreationDtoToImage(ImageCreationDTO imageCreationDTO) {
        return modelMapper.typeMap(ImageCreationDTO.class, Image.class)
                .addMapping(imgDto -> {
                    try {
                        return imgDto.getFile().getBytes();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    return imgDto;
                }, Image::setFile).map(imageCreationDTO);
    }

    @Override
    public ImageResponseDTO imageToImageResponseDto(Image image) {
        return modelMapper.typeMap(Image.class, ImageResponseDTO.class).map(image);
    }

    @Override
    public Image ImageUpdateDtoToImage(ImageUpdateDTO imageUpdateDTO) {
        return modelMapper.typeMap(ImageUpdateDTO.class, Image.class)
                .addMapping(imgDto -> {
                    try {
                        return imgDto.getFile().getBytes();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    return imgDto;
                }, Image::setFile).map(imageUpdateDTO);
    }

    @Override
    public Image imageToImage(Image image1, Image image2) {
        modelMapper.typeMap(Image.class, Image.class).addMappings(mpr -> mpr.skip(Image::setId))
                .map(image1, image2);
        System.out.println(image2);
        return image2;
    }
}
