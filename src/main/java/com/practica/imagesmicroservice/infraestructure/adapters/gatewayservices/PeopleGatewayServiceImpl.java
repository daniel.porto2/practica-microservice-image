package com.practica.imagesmicroservice.infraestructure.adapters.gatewayservices;

import com.practica.imagesmicroservice.domain.ports.PeopleGatewayService;
import com.practica.imagesmicroservice.infraestructure.gateways.PeopleGateway;

public class PeopleGatewayServiceImpl implements PeopleGatewayService {

    private final PeopleGateway peopleGateway;

    public PeopleGatewayServiceImpl(PeopleGateway peopleGateway) {
        this.peopleGateway = peopleGateway;
    }

    @Override
    public Boolean doesPersonIdExists(String personId) {
        Boolean result = peopleGateway.doesPersonIdExists(personId);
        if(result == null){
            throw new RuntimeException("Something was wrong, try in a while again");
        }
        return result;
    }
}
