package com.practica.imagesmicroservice.infraestructure.adapters.respositoryservices;

import com.practica.imagesmicroservice.domain.entity.image.Image;
import com.practica.imagesmicroservice.domain.ports.ImageRepositoryService;
import com.practica.imagesmicroservice.infraestructure.persistence.entities.ImagePersistenceEntity;
import com.practica.imagesmicroservice.infraestructure.persistence.mappers.ImagePersistenceMapper;
import com.practica.imagesmicroservice.infraestructure.persistence.repositories.ImageRepository;

public class ImageRepositoryServiceImpl implements ImageRepositoryService {

    private final ImageRepository imageRepository;

    private final ImagePersistenceMapper imagePersistenceMapper;

    public ImageRepositoryServiceImpl(ImageRepository imageRepository, ImagePersistenceMapper imagePersistenceMapper) {
        this.imageRepository = imageRepository;
        this.imagePersistenceMapper = imagePersistenceMapper;
    }

    @Override
    public Image saveImage(Image image) {
        ImagePersistenceEntity ipe = imagePersistenceMapper.imageToImagePersistenceEntity(image);
        ipe = imageRepository.save(ipe);
        return imagePersistenceMapper.imagePersistenceEntityToImage(ipe);
    }

    @Override
    public Image getImage(Image image) {
        ImagePersistenceEntity ipe = imageRepository.findByPersonId(Long.parseLong(image.getPersonId())).get();
        return imagePersistenceMapper.imagePersistenceEntityToImage(ipe);
    }

    @Override
    public void updateImage(Image image) {
        ImagePersistenceEntity ipe = imagePersistenceMapper.imageToImagePersistenceEntity(image);
        imageRepository.save(ipe);
    }

    @Override
    public void deleteImage(Image image) {
        imageRepository.deleteByPersonId(Long.parseLong(image.getPersonId()));
    }

    @Override
    public boolean doesPersonIdHaveImage(String personId) {
        return imageRepository.existsByPersonId(Long.parseLong(personId));
    }
}
