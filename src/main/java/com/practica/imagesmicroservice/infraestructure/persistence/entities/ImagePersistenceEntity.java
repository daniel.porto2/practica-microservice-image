package com.practica.imagesmicroservice.infraestructure.persistence.entities;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "images")
@Getter
@Setter
public class ImagePersistenceEntity {
    @Id
    private String id;

    @Indexed(unique = true)
    private long personId;

    private byte[] file;
}
