package com.practica.imagesmicroservice.infraestructure.persistence.mappers;

import com.practica.imagesmicroservice.domain.entity.image.Image;
import com.practica.imagesmicroservice.infraestructure.persistence.entities.ImagePersistenceEntity;
import org.modelmapper.ModelMapper;

public class ImagePersistenceMapperImpl implements ImagePersistenceMapper {

    private final ModelMapper modelMapper;

    public ImagePersistenceMapperImpl(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    @Override
    public ImagePersistenceEntity imageToImagePersistenceEntity(Image image) {
        return modelMapper.typeMap(Image.class, ImagePersistenceEntity.class).map(image);
    }


    @Override
    public Image imagePersistenceEntityToImage(ImagePersistenceEntity imagePersistenceEntity) {
        return modelMapper.typeMap(ImagePersistenceEntity.class, Image.class).map(imagePersistenceEntity);
    }
}
