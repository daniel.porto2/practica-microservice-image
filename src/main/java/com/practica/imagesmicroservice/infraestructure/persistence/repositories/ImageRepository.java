package com.practica.imagesmicroservice.infraestructure.persistence.repositories;

import com.practica.imagesmicroservice.infraestructure.persistence.entities.ImagePersistenceEntity;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface ImageRepository extends MongoRepository<ImagePersistenceEntity, String> {
    public Optional<ImagePersistenceEntity> findByPersonId(Long personId);

    public boolean existsByPersonId(Long personId);

    public void deleteByPersonId(Long personId);
}
