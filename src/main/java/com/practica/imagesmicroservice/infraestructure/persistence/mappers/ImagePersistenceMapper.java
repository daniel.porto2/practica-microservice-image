package com.practica.imagesmicroservice.infraestructure.persistence.mappers;

import com.practica.imagesmicroservice.domain.entity.image.Image;
import com.practica.imagesmicroservice.infraestructure.persistence.entities.ImagePersistenceEntity;

public interface ImagePersistenceMapper {
    public ImagePersistenceEntity imageToImagePersistenceEntity(Image image);

    public Image imagePersistenceEntityToImage(ImagePersistenceEntity image);
}
