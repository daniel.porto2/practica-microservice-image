package com.practica.imagesmicroservice.infraestructure.delivery;

import com.practica.imagesmicroservice.application.dtos.image.ImageCreationDTO;
import com.practica.imagesmicroservice.application.dtos.image.ImageResponseDTO;
import com.practica.imagesmicroservice.application.dtos.image.ImageUpdateDTO;
import com.practica.imagesmicroservice.application.services.ImageApplicationService;
import com.practica.imagesmicroservice.infraestructure.shared.util.Routes;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;

@RestController
@RequestMapping(Routes.imageRoute)
public class imageControllerImpl implements ImageController{

    private final ImageApplicationService imageApplicationService;

    public imageControllerImpl(ImageApplicationService imageApplicationService) {
        this.imageApplicationService = imageApplicationService;
    }

    @Override
    @PostMapping
    public ResponseEntity<ImageResponseDTO> addImage(@PathVariable("personId") String personId, @RequestPart MultipartFile file, @ModelAttribute ImageCreationDTO imageCreationDTO) {
        ImageResponseDTO image= imageApplicationService.addImage(personId, imageCreationDTO);
        return new ResponseEntity<ImageResponseDTO>(image, HttpStatus.CREATED);
    }

    @Override
    @GetMapping
    public ResponseEntity<ImageResponseDTO> getImage(@PathVariable("personId") String personId) {
        ImageResponseDTO image = imageApplicationService.getImageByPersonId(personId);
        return new ResponseEntity<ImageResponseDTO>(image, HttpStatus.OK);
    }

    @Override
    @PutMapping
    public ResponseEntity<Void> updateImage(@PathVariable("personId") String personId, @RequestPart MultipartFile file, @Valid ImageUpdateDTO imageUpdateDTO) {
        imageApplicationService.updateImage(personId, imageUpdateDTO);
        return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
    }

    @Override
    @DeleteMapping
    public ResponseEntity<Void> deleteImage(@PathVariable("personId") String personId) {
        imageApplicationService.deleteImageByPersonId(personId);
        return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
    }
}
