package com.practica.imagesmicroservice.infraestructure.delivery;

import com.practica.imagesmicroservice.application.dtos.image.ImageCreationDTO;
import com.practica.imagesmicroservice.application.dtos.image.ImageResponseDTO;
import com.practica.imagesmicroservice.application.dtos.image.ImageUpdateDTO;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

public interface ImageController {
    public ResponseEntity<ImageResponseDTO> addImage(String personId, MultipartFile file, ImageCreationDTO imageCreationDTO);

    public ResponseEntity<ImageResponseDTO> getImage(String personId);

    public ResponseEntity<Void> updateImage(String personId, MultipartFile file, ImageUpdateDTO imageUpdateDTO);

    public ResponseEntity<Void> deleteImage(String personId);
}
