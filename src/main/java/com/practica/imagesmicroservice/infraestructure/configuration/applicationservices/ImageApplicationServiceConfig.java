package com.practica.imagesmicroservice.infraestructure.configuration.applicationservices;

import com.practica.imagesmicroservice.application.ports.ImageMapperService;
import com.practica.imagesmicroservice.application.services.ImageApplicationService;
import com.practica.imagesmicroservice.application.services.ImageApplicationServiceImpl;
import com.practica.imagesmicroservice.infraestructure.adapters.mappers.ImageMapperServiceImpl;
import com.practica.imagesmicroservice.infraestructure.configuration.usecases.ImageUseCasesConfig;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ImageApplicationServiceConfig {
    private ModelMapper modelMapper;

    private ImageUseCasesConfig imageCrudUseCaseConfig;

    private ImageMapperService imageMapperService;

    @Autowired
    public ImageApplicationServiceConfig(ModelMapper modelMapper, ImageUseCasesConfig imageCrudUseCaseConfig) {
        this.modelMapper = modelMapper;
        this.imageCrudUseCaseConfig = imageCrudUseCaseConfig;
        this.imageMapperService = new ImageMapperServiceImpl(modelMapper);
    }

    @Bean
    public ImageApplicationService createImageApplicationService() {
        return new ImageApplicationServiceImpl(imageMapperService,
                imageCrudUseCaseConfig.addImageUseCase(),
                imageCrudUseCaseConfig.getImageUseCase(),
                imageCrudUseCaseConfig.updateImageUseCase(),
                imageCrudUseCaseConfig.deleteImageUseCase());
    }
}
