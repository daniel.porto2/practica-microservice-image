package com.practica.imagesmicroservice.infraestructure.configuration.usecases;

import com.practica.imagesmicroservice.domain.ports.ImageRepositoryService;
import com.practica.imagesmicroservice.domain.ports.PeopleGatewayService;
import com.practica.imagesmicroservice.domain.usecases.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ImageUseCasesConfig {

    private ImageRepositoryService imageRepositoryService;

    private PeopleGatewayService peopleGatewayService;

    @Autowired
    public ImageUseCasesConfig(ImageRepositoryService imageRepositoryService, PeopleGatewayService peopleGatewayService){
        this.imageRepositoryService = imageRepositoryService;
        this.peopleGatewayService = peopleGatewayService;
    }

    public AddImageUseCase addImageUseCase(){
        return new AddImageUseCaseImpl(imageRepositoryService, peopleGatewayService);
    }

    public GetImageUseCase getImageUseCase() {
        return new GetImageUseCaseImpl(imageRepositoryService);
    }

    public UpdateImageUseCase updateImageUseCase() {
        return new UpdateImageUseCaseImpl(imageRepositoryService);
    }

    public DeleteImageUseCase deleteImageUseCase() {
        return new DeleteImageUseCaseImpl(imageRepositoryService);
    }
}
