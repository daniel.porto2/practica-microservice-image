package com.practica.imagesmicroservice.infraestructure.configuration.repositoryservices;

import com.practica.imagesmicroservice.domain.ports.ImageRepositoryService;
import com.practica.imagesmicroservice.infraestructure.adapters.respositoryservices.ImageRepositoryServiceImpl;
import com.practica.imagesmicroservice.infraestructure.persistence.mappers.ImagePersistenceMapper;
import com.practica.imagesmicroservice.infraestructure.persistence.mappers.ImagePersistenceMapperImpl;
import com.practica.imagesmicroservice.infraestructure.persistence.repositories.ImageRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ImageRepositoryServiceConfig {

    private ModelMapper modelMapper;

    private ImageRepository imageRepository;

    @Autowired
    public ImageRepositoryServiceConfig(ModelMapper modelMapper, ImageRepository imageRepository) {
        this.modelMapper = modelMapper;
        this.imageRepository = imageRepository;
    }

    public ImagePersistenceMapper createPersonPersistenceMapper() {
        return new ImagePersistenceMapperImpl(modelMapper);
    }

    public ImagePersistenceMapper createImagePersistenceMapper() {
        return new ImagePersistenceMapperImpl(modelMapper);
    }

    @Bean
    public ImageRepositoryService createImageRepositoryService() {
        return new ImageRepositoryServiceImpl(imageRepository, createImagePersistenceMapper());
    }

}
