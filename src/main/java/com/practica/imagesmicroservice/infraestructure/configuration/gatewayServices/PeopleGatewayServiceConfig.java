package com.practica.imagesmicroservice.infraestructure.configuration.gatewayServices;

import com.practica.imagesmicroservice.domain.ports.PeopleGatewayService;
import com.practica.imagesmicroservice.infraestructure.adapters.gatewayservices.PeopleGatewayServiceImpl;
import com.practica.imagesmicroservice.infraestructure.gateways.PeopleGateway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class PeopleGatewayServiceConfig {

    private PeopleGateway peopleGateway;

    @Autowired
    public PeopleGatewayServiceConfig(PeopleGateway peopleGateway) {
        this.peopleGateway = peopleGateway;
    }

    @Bean
    public PeopleGatewayService createPeopleGatewayService(){
        return new PeopleGatewayServiceImpl(peopleGateway);
    }

}
