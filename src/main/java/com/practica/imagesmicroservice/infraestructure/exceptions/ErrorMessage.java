package com.practica.imagesmicroservice.infraestructure.exceptions;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.converter.HttpMessageNotReadableException;

import java.time.LocalDateTime;

@Getter
@Setter
public class ErrorMessage {
    private final String error;
    private final String message;
    private final Integer code;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy hh:mm:ss")
    private LocalDateTime timestamp;

    ErrorMessage(Exception exception, Integer code) {
        this.timestamp = LocalDateTime.now();
        this.error = exception.getClass().getSimpleName();
        this.message = generateMessage(exception);
        this.code = code;
    }

    public String generateMessage(Exception exception) {
        if(exception instanceof HttpMessageNotReadableException) {
            String errorClassName = ((HttpMessageNotReadableException) exception).getRootCause().getClass().getSimpleName();
            switch (errorClassName) {
                case "InvalidFormatException":
                    return "One of the properties of the body request has not the right format";

                case "JsonParseException":
                    return "the body request is not a valid JSON";

                case "NullPointerException":
                    return "One of the required properties is null or empty";
            }
        }
        return exception.getMessage();
    }
}
