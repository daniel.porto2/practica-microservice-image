package com.practica.imagesmicroservice.infraestructure.shared.util;

public class PeopleGatewayRoutes {
    public static final String baseRoute = "/api/v1/people";
    public static final String doesPersonIdExistsRoute = baseRoute+"/{id}/exists";
}
