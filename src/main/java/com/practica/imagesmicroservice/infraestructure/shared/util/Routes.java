package com.practica.imagesmicroservice.infraestructure.shared.util;

public class Routes {
    public static final String baseRoute = "/api/v1";
    public static final String personsRoute = baseRoute+"/people";
    public static final String imageRoute = personsRoute+"/{personId}/image";
}
