package com.practica.imagesmicroservice.infraestructure.gateways;

import com.practica.imagesmicroservice.infraestructure.shared.util.PeopleGatewayRoutes;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(name="people-microservice", fallback = PeopleGateway.PeopleGatewayFallBack.class)
public interface PeopleGateway {
    @RequestMapping(method = RequestMethod.GET, value = PeopleGatewayRoutes.doesPersonIdExistsRoute, produces = MediaType.APPLICATION_JSON_VALUE)
    Boolean doesPersonIdExists(@PathVariable("id") String id);

    @Component
    class PeopleGatewayFallBack implements PeopleGateway{

        @Override
        public Boolean doesPersonIdExists(String id) {
            return null;
        }
    }
}
