package com.practica.imagesmicroservice.domain.usecases;

import com.practica.imagesmicroservice.data.ImageData;
import com.practica.imagesmicroservice.domain.entity.image.Image;
import com.practica.imagesmicroservice.domain.ports.ImageRepositoryService;
import com.practica.imagesmicroservice.domain.ports.PeopleGatewayService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.when;

class AddImageUseCaseImplTest {

    @Mock
    private ImageRepositoryService imageRepositoryService;

    @Mock
    private PeopleGatewayService peopleGatewayService;

    @InjectMocks
    private AddImageUseCaseImpl addImageUseCase;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void execute() {
        when(peopleGatewayService.doesPersonIdExists("0")).thenReturn(true);
        when(imageRepositoryService.doesPersonIdHaveImage("0")).thenReturn(false);
        when(imageRepositoryService.saveImage(any(Image.class))).thenReturn(ImageData.IMAGE8);

        Image savedImage = addImageUseCase.execute(ImageData.NEW_IMAGE);
        assertEquals("8", savedImage.getId());
    }
}