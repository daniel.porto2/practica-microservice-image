package com.practica.imagesmicroservice.domain.usecases;

import com.practica.imagesmicroservice.data.ImageData;
import com.practica.imagesmicroservice.domain.ports.ImageRepositoryService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class DeleteImageUseCaseImplTest {
    @Mock
    private ImageRepositoryService imageRepositoryService;

    @InjectMocks
    private DeleteImageUseCaseImpl deleteImageUseCase;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void execute() {
        when(imageRepositoryService.doesPersonIdHaveImage("0")).thenReturn(true);
        deleteImageUseCase.execute(ImageData.IMAGE_WITH_ONLY_PERSONID);
        verify(imageRepositoryService, times(1)).deleteImage(ImageData.IMAGE_WITH_ONLY_PERSONID);
    }
}