package com.practica.imagesmicroservice.domain.usecases;

import com.practica.imagesmicroservice.data.ImageData;
import com.practica.imagesmicroservice.domain.entity.image.Image;
import com.practica.imagesmicroservice.domain.ports.ImageRepositoryService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class GetImageUseCaseImplTest {
    @Mock
    private ImageRepositoryService imageRepositoryService;

    @InjectMocks
    private  GetImageUseCaseImpl getImageUseCase;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void execute() {
        when(imageRepositoryService.doesPersonIdHaveImage("0")).thenReturn(true);
        when(imageRepositoryService.getImage(ImageData.IMAGE_WITH_ONLY_PERSONID)).thenReturn(ImageData.IMAGE8);

        Image image = getImageUseCase.execute(ImageData.IMAGE_WITH_ONLY_PERSONID);
        assertEquals("8", image.getId());
        verify(imageRepositoryService, times(1)).getImage(ImageData.IMAGE_WITH_ONLY_PERSONID);
    }
}