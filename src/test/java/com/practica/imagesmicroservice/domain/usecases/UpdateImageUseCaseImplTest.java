package com.practica.imagesmicroservice.domain.usecases;

import com.practica.imagesmicroservice.data.ImageData;
import com.practica.imagesmicroservice.domain.ports.ImageRepositoryService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class UpdateImageUseCaseImplTest {
    @Mock
    private ImageRepositoryService imageRepositoryService;

    @InjectMocks
    private  UpdateImageUseCaseImpl updateImageUseCase;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void execute() {
        when(imageRepositoryService.doesPersonIdHaveImage("0")).thenReturn(true);
        updateImageUseCase.execute(ImageData.NEW_IMAGE);
        verify(imageRepositoryService, times(1)).updateImage(ImageData.NEW_IMAGE);
    }
}