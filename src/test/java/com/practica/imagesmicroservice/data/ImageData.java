package com.practica.imagesmicroservice.data;

import com.practica.imagesmicroservice.domain.entity.image.Image;

import java.nio.charset.StandardCharsets;

public class ImageData {
    public static Image NEW_IMAGE = new Image(null, "e04fd020ea3a6910a2d808002b30309d".getBytes(StandardCharsets.UTF_8), "0");

    public static Image IMAGE8 = new Image("8", "F43RTF8374F853445F4346F46F".getBytes(StandardCharsets.UTF_8), "0");

    public static Image IMAGE_WITH_ONLY_PERSONID = new Image(null, null, "0");
}
